
const controller = require('../controllers'); 
module.exports = (app) => {
    
     app
    .get('/api/users', controller.userslist)
    .post('/api/users',controller.createUser);
    
}