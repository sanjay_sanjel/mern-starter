const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());


require('./routes/')(app);

//Handing of simple homepage
app.get('/',(req,res)=>{
  res.send("This is home page");
})

//Handling of other pages
app.get('*',(req,res)=>{
	res.send("Error 404 not found");
})

app.listen(3000, () => console.log("Listening to port 3000"));